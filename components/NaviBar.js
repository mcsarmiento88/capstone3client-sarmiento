import React, { useContext } from "react";
import Link from "next/link";
import { Navbar, Nav } from "react-bootstrap";
import UserContext from "../UserContext";

export default function NavBar() {
    const { user } = useContext(UserContext);

    return (
        <Navbar bg="light" expand="lg">
            <Link href="/records">
                <a className="navbar-brand">Money Manager</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    {user.id !== null ? (
                        <React.Fragment>
                            <Link href="/categories">
                                <a className="nav-link" role="button">
                                    Categories
                                </a>
                            </Link>
                            <Link href="/records">
                                <a className="nav-link" role="button">
                                    Records
                                </a>
                            </Link>
                            <Link href="/charts/monthlyExpense">
                                <a className="nav-link" role="button">
                                    Monthly Expense
                                </a>
                            </Link>
                            <Link href="/charts/monthlyIncome">
                                <a className="nav-link" role="button">
                                    Monthly Income
                                </a>
                            </Link>
                            <Link href="/logout">
                                <a className="nav-link" role="button">
                                    Logout
                                </a>
                            </Link>
                        </React.Fragment>
                    ) : (
                        <React.Fragment>
                            <Link href="/login">
                                <a className="nav-link" role="button">
                                    Login
                                </a>
                            </Link>
                            <Link href="/register">
                                <a className="nav-link" role="button">
                                    Register
                                </a>
                            </Link>
                        </React.Fragment>
                    )}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}
