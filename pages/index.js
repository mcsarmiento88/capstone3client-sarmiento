import Head from 'next/head'
import styles from '../styles/index.module.css'

export default function Home() {
  return (
  	<div className={styles.center}>
    	<h1 className={styles.text}>Welcome to My Money Manager</h1>
    </div>
  )
}
