import React, { useState, useEffect,useContext } from 'react';
import Router from 'next/router';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';
import styles from '../../styles/index.module.css'


import {GoogleLogin} from 'react-google-login'

// clientId="496138713653-so7ikfbtbskaf669u6p5m6qam2m0dd97.apps.googleusercontent.com"
export default function index() {
    const {user,setUser} = useContext(UserContext)  
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {

        e.preventDefault();
        fetch(' https://serene-dawn-26142.herokuapp.com/api/users/login', {
            method: "POST",
            headers: {
                'Content-Type': "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data.accessToken){

                localStorage.setItem('token', data.accessToken)

               
                fetch(' https://serene-dawn-26142.herokuapp.com/api/users/details',{

                    headers: {

                        Authorization: `Bearer ${data.accessToken}`

                    }

                })
                .then(res => res.json())
                .then(data => {

               
                    localStorage.setItem('id',data._id)
                    

                    setUser({

                        id: data._id,
                        

                    })
                })

                setEmail('');
                setPassword('');

                Swal.fire({
                    icon: "success",
                    title: "Successfully logged in.",
                    text: "Thank you for logging in."
                })

                
                Router.push('/records')

            } else {

                Swal.fire({
                    icon: "error",
                    title: "Unsuccessful.",
                    text: "User authentication failed."
                })

            }

        })

    }

    useEffect(() => {

        
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    
    function authenticateGoogleToken(response){

        console.log(response)

        fetch(' https://serene-dawn-26142.herokuapp.com/api/users/verify-google-id-token',{

            method: 'POST',
            headers: {

                'Content-Type': 'application/json'

            },
            body: JSON.stringify({

                tokenId: response.tokenId

            })

        })
        .then(res => res.json())
        .then(data => {
            
            console.log(data);

            

            if (typeof data.accessToken !== 'undefined') {

                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken);

            } else {

                if(data.error === 'google-auth-error'){
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed',
                        'error'
                    )
                } else if (data.error === 'login-type-error'){
                    Swal.fire(
                        'Login Type Error',
                        'You may have registered through a different login procedure',
                        'error'
                    )
                }

            }
        })

    }

    function retrieveUserDetails(accessToken) {

        fetch('https://serene-dawn-26142.herokuapp.com/api/users/details', {
            headers: { Authorization: `Bearer ${accessToken}` }
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            localStorage.setItem('id', data._id)
            
            setUser({
                id: data._id,
                
            })

 
            
            Router.push('/records')
            
        

        })


    }

    

    return (
        <div className={styles.center}>
            <Form onSubmit={(e) => authenticate(e)} className={styles.center2}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            {isActive ? 
                <Button variant="primary" type="submit" id="submitBtn" className="btn-block">
                    Submit
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" disabled className="btn-block">
                    Submit
                </Button>
            }

            <GoogleLogin 

                clientId="496138713653-so7ikfbtbskaf669u6p5m6qam2m0dd97.apps.googleusercontent.com"
                buttonText="Login Using Google"
                onSuccess={authenticateGoogleToken}
                onFailure={authenticateGoogleToken}
                cookiePolicy={'single_host_origin'}
                className="w-100 text-center my-4 d-flex justify-content-center"
            />
        </Form>
        </div>
        
    )
}
