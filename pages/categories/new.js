import React, {useState,useEffect} from 'react'
import { Form, Button, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Router from 'next/router'

export default function newCategories(){

	
	const [name,setName] = useState("")
	const [type,setType] = useState("")
	
	const [isActive,setIsActive] = useState(true)

	
	useEffect(()=>{

		if(name !== "" && type !== ""){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[name, type]) 
	

	function addCategory(e){

		e.preventDefault()

		let token = localStorage.getItem('token')
		fetch(' https://serene-dawn-26142.herokuapp.com/api/categories/',{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`

			},
			body: JSON.stringify({

				name: name,
				type: type,

			})
		})
		.then(res => res.json())
		.then(data => {

			
			if(data){

				Swal.fire({

					icon: 'success',
					title: 'Category Added',
					text: 'Successfully Added Category'

				})

				Router.push('/categories')
				

			} else {

				Swal.fire({

					icon: 'error',
					title: 'Adding Category Failed.',
					text: 'There has been an internal server error.'

				})

			}
 

		})


		setName("")
		setType("")
		

	}
	return (
		<React.Fragment>
			<h1>New Category</h1>
			<Card>
				<Card.Header>Category Information</Card.Header>
				<Card.Body>
					<Form onSubmit={e => addCategory(e)}>
						<Form.Group>
							<Form.Label>Category Name:</Form.Label>
							<Form.Control
								type="text"
								value={name}
								onChange={e => setName(e.target.value)}
								placeholder="Enter category name"
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Category Type:</Form.Label>
							<Form.Control
								as="select"
								value={type}
								onChange={e => setType(e.target.value)}
								required
							>
								<option disabled value="">
									Select Category
								</option>
								<option>Income</option>
								<option>Expense</option>
							</Form.Control>
						</Form.Group>
						{isActive ? (
							<Button
								variant="success"
								type="submit"
								id="submitBtn"
							>
								Submit
							</Button>
						) : (
							<Button
								variant="success"
								type="submit"
								id="submitBtn"
								disabled
							>
								Submit
							</Button>
						)}
					</Form>
				</Card.Body>
			</Card>
		</React.Fragment>
		)

}
